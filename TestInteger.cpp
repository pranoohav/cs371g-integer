// ---------------
// TestInteger.c++
// ---------------

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Integer.hpp"

using namespace std;

// ----
// constructor
// ----

TEST(IntegerFixture, constructor1) {
    Integer<int> x = 1234;
    ASSERT_EQ(x, 1234);
}

TEST(IntegerFixture, constructor2) {
    Integer<int> x = Integer<int>("1234");
    ASSERT_EQ(x, 1234);
}

TEST(IntegerFixture, constructor3) {
    Integer<int> x = Integer<int>("7342564754387843");
    ASSERT_EQ(x, 7342564754387843);
}

// ----
// plus
// ----

TEST(IntegerFixture, plus1) {
    Integer<int> x = 2;
    Integer<int> y = 3;
    Integer<int> z = x + y;	
	ASSERT_EQ(z.to_integer(), 5);
}

TEST(IntegerFixture, plus2) {
    Integer<int> x = -310;
    Integer<int> y = 211;
    Integer<int> z = x + y;	
	ASSERT_EQ(z.to_integer(), -99);
}

TEST(IntegerFixture, plus3) {
    Integer<int> x = -12;
    Integer<int> y = -46;
    Integer<int> z = x + y;	
	ASSERT_EQ(z.to_integer(), -58);
}

// ---
// subtract
// ---

TEST(IntegerFixture, minus1) {
    Integer<int> x = 10;
    Integer<int> y = 6;
    Integer<int> z = x - y;
    ASSERT_EQ(z.to_integer(), 4);
}

TEST(IntegerFixture, minus2) {
    Integer<int> x = 1432;
    Integer<int> y = 709;
    Integer<int> z = x - y;
    ASSERT_EQ(z.to_integer(), 723);
}

TEST(IntegerFixture, minus3) {
    Integer<int> x = 5449;
    Integer<int> y = 232515;
    Integer<int> z = x - y;
    ASSERT_EQ(z.to_integer(), -227066);
}

// ---
// multiply
// ---

TEST(IntegerFixture, multiply1) {
    Integer<int> x = -70;
    Integer<int> y = 21;
    Integer<int> z = x * y;
    ASSERT_EQ(z.to_integer(), -1470);
}

TEST(IntegerFixture, multiply2) {
    Integer<int> x = -13;
    Integer<int> y = -51;
    Integer<int> z = x * y;
    ASSERT_EQ(z.to_integer(), 663);
}

TEST(IntegerFixture, multiply3) {
    Integer<int> x = 14728;
    Integer<int> y = 612;
    Integer<int> z = x * y;
    ASSERT_EQ(z.to_integer(), 9013536);
}

// ---
// pow
// ---

TEST(IntegerFixture, pow1) {
    Integer<int> x = 2;
    Integer<int> y = 4;
    Integer<int> z = x.pow(y.to_integer());
    ASSERT_EQ(z.to_integer(), 16);
}

TEST(IntegerFixture, pow2) {
    Integer<int> x = 6;
    Integer<int> y = 3;
    Integer<int> z = x.pow(y.to_integer());
    ASSERT_EQ(z.to_integer(), 216);
}

TEST(IntegerFixture, pow3) {
    Integer<int> x = 5;
    Integer<int> y = 1;
    Integer<int> z = x.pow(y.to_integer());
    ASSERT_EQ(z.to_integer(), 5);
}