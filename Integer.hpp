// ---------
// Integer.h
// ---------

#ifndef Integer_h
#define Integer_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <iostream>  // ostream
#include <stdexcept> // invalid_argument
#include <string>    // string
#include <vector>    // vector
#include <bits/stdc++.h> 

using namespace std;
// -----------------
// shift_left_digits
// -----------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift left of the input sequence into the output sequence
 * ([b, e) << n) => x
 */
template <typename II, typename FI>
FI shift_left_digits (II b, II e, int n, FI x) {
    // <your code>
    while (b != e) {
        *x = *b;
        ++b;
        ++x;
    }
    for (int i = 0; i < n; i++) {
        *x = 0;
        ++x;
    }
    return x;}

// ------------------
// shift_right_digits
// ------------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift right of the input sequence into the output sequence
 * ([b, e) >> n) => x
 */
template <typename II, typename FI>
FI shift_right_digits (II b, II e, int n, FI x) {
    // <your code>
    int d = distance(b, e);
    d -= n;
    for (int i = 0; i < d; i++) {
        *x = *b;
        ++x;
        ++b;
    }
    return x;}

// -----------
// plus_digits
// -----------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the sum of the two input sequences into the output sequence
 * ([b1, e1) + [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI plus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    // <your code>
    // Consider negative _x
    int carry = 0;
    while (b1 != e1 && b2 != e2) {
        *x = *b1 + *b2;
		*x += carry;
		carry = 0;
		if (*x >= 10) {
            *x -= 10;
            carry = 1;
        }
        ++x;
        ++b1;
        ++b2;          
    }
    while(b1 != e1){		
		*x = *b1;
        *x += carry;
		carry = 0;
        if (*x >= 10) {
            *x -= 10;
            carry = 1;
        }
		++b1;
        ++x;
	}
	while(b2 != e2){
		*x = *b2;
        *x += carry;
		carry = 0;
        if (*x >= 10) {
            *x -= 10;
            carry = 1;
        }
		++b2;
        ++x;
	}
	if (carry == 1) {
            *x = 1;
    }
	return x;}

// ------------
// minus_digits
// ------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the difference of the two input sequences into the output sequence
 * ([b1, e1) - [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI minus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    int carry = 0;
    while (b1 != e1 && b2 != e2) {
        *x = *b1 - *b2;
        *x -= carry;
        carry = 0;        
        if (*x < 0) {
            *x += 10;
            carry = 1;
        }
        ++x;
        ++b1;
        ++b2;
    }
    while(b1 != e1){		
        *x = *b1;
        *x -= carry;
        carry = 0;
        if (*x < 0) {
            *x += 10;
            carry = 1;
        }
        b1++;
        x++;
        }
    return x;}

// -----------------
// multiplies_digits
// -----------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the product of the two input sequences into the output sequence
 * ([b1, e1) * [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI multiplies_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    // <your code>
    // Consider negatives
    II2 b1_head = b1;
    FI x_head = x;
    int carry = 0;
    while (b2 != e2) {
        while (b1 != e1) {
            *x += (*b1) * (*b2);
            *x += carry;
            carry = 0;
            if (*x >= 10) {
                carry = *x / 10;
                *x = *x % 10;
            }
            ++x;
            ++b1;
        }
        while (carry != 0){
			*x += carry;
            carry = 0;
            if (*x >= 10) {
                carry = *x / 10;
                *x = *x % 10;
            }
            ++x;
		}
		++x_head;
        x = x_head;
        b1 = b1_head;
        ++b2;
    }
    if(carry != 0){
		*x += carry;
	}
	return x;}

    // bool not_a_digit(char c) {

    // }

// -------
// Integer
// -------

template <typename T, typename C = vector<T>>
class Integer {
    // -----------
    // operator ==
    // -----------

    /**
     * your documentation
     */
    friend bool operator == (const Integer& lhs, const Integer& rhs) {
        // <your code>
        if(lhs.negative != rhs.negative){
			return false;
		}
		bool lzero = true;
		int lIndex = -1;
		bool rzero = true;
		int rIndex = -1;
		while (lzero)
		{
			lIndex++;
            if(lhs.container[lIndex] != 0){
                lzero = false;
			}
		}
        while (rzero)
		{
			rIndex++;
            if(rhs.container[rIndex] != 0){
                rzero = false;
			}
		}
        while((lIndex < lhs.container.size()) && (rIndex < rhs.container.size())){
            if(lhs.container[lIndex] != rhs.container[rIndex]){
				return false;
			}
            lIndex++;
            rIndex++;
		}
        if((lIndex < lhs.container.size()) || (rIndex < rhs.container.size())){
            return false;
		}		
        return true;
	}

    // -----------
    // operator !=
    // -----------

    /**
     * your documentation
     */
    friend bool operator != (const Integer& lhs, const Integer& rhs) {
        return !(lhs == rhs);}

    // ----------
    // operator <
    // ----------

    /**
     * your documentation
     */
    friend bool operator < (const Integer& lhs, const Integer& rhs) {
        // <your code>        
        if(lhs.negative && !rhs.negative){
            return true;
		}
        if(!lhs.negative && rhs.negative){
            return false;
		}
        if(!lhs.negative){
            if (lhs.container.size() > rhs.container.size()) {                
                return false;
            }
            if (lhs.container.size() < rhs.container.size()) {               
                return true;
            }
            for (long int i = lhs.container.size() - 1; i >= 0; i--) {                
                if (lhs.container[i] < rhs.container[i]) {                   
                    return true;
                }
                if(lhs.container[i] > rhs.container[i]){
					return false;;
				}
			}
		}
        if(lhs.negative){
            if (lhs.container.size() < rhs.container.size()) {                
                return false;
            }
            if (lhs.container.size() > rhs.container.size()) {                
                return true;
            }
            for (long int i = lhs.container.size() - 1; i >= 0; i--) {
                if (lhs.container[i] > rhs.container[i]) {                    
                    return true;
                }
                if(lhs.container[i] < rhs.container[i]){
					return false;
				} 
            }
		}		
        return false;}

    // -----------
    // operator <=
    // -----------

    /**
     * your documentation
     */
    friend bool operator <= (const Integer& lhs, const Integer& rhs) {
        return !(rhs < lhs);}

    // ----------
    // operator >
    // ----------

    /**
     * your documentation
     */
    friend bool operator > (const Integer& lhs, const Integer& rhs) {
        return (rhs < lhs);}

    // -----------
    // operator >=
    // -----------

    /**
     * your documentation
     */
    friend bool operator >= (const Integer& lhs, const Integer& rhs) {
        return !(lhs < rhs);}

    // ----------
    // operator +
    // ----------

    /**
     * your documentation
     */
    friend Integer operator + (Integer lhs, const Integer& rhs) {
        return lhs += rhs;}

    // ----------
    // operator -
    // ----------

    /**
     * your documentation
     */
    friend Integer operator - (Integer lhs, const Integer& rhs) {
        return lhs -= rhs;}

    // ----------
    // operator *
    // ----------

    /**
     * your documentation
     */
    friend Integer operator * (Integer lhs, const Integer& rhs) {
        return lhs *= rhs;}

    // -----------
    // operator <<
    // -----------

    /**
     * your documentation
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator << (Integer lhs, int rhs) {
        return lhs <<= rhs;}

    // -----------
    // operator >>
    // -----------

    /**
     * your documentation
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator >> (Integer lhs, int rhs) {
        return lhs >>= rhs;}

    // -----------
    // operator <<
    // -----------

    /**
     * your documentation
     */
    friend std::ostream& operator << (std::ostream& lhs, const Integer& rhs) {
        // <your code>
        if(rhs.negative){
			lhs << '-';
		}
        bool leading = false;        
        int pos = rhs.container.size() - 1;
        if(rhs.container[pos] == 0){
			leading = true;
		}
		while(leading){
            if(rhs.container[pos] != 0){
                leading = false;
				pos++;
			}
			pos--;
		}
		for (int i = pos; i >= 0; i--){
			lhs << rhs.container[i];
		}
		return lhs;
	}

	// ---
    // abs
    // ---

    /**
     * absolute value
     * your documentation
     */
    friend Integer abs (Integer x) {
        return x.abs();}

    // ---
    // pow
    // ---

    /**
     * power
     * your documentation
     * @throws invalid_argument if ((x == 0) && (e == 0)) || (e < 0)
     */
    friend Integer pow (Integer x, int e) {
        return x.pow(e);}

    private:
        // ----
        // data
        // ----
        C _x; // the backing container                
    private:
        // -----
        // valid
        // -----

        bool valid () const { // class invariant
            // <your code>
            if (container.empty()){
				return false;
			}
            for (int i: container){
                if (i < 0 || i > 9){
					return false;
				}
			}
			return true;}

    public:
        // ------------
        // constructors
        // ------------
        vector<T> container;
        bool negative;
        /**
         * your documentation
         */

        Integer (long long value) {
            // <your code>
            assert(&value);
            negative = false;
            if (value == 0) {
                container.push_back(value);
            } 
            if (value < 0) {
                negative = true;
                value = value * -1;
            }
            while (value > 0) {
                int digit = value % 10;
                container.push_back(digit);
                value /= 10;
            }
            assert(valid());}

        /**
         * your documentation
         * @throws invalid_argument if value is not a valid representation of an Integer
         */
        explicit Integer (const string &value) {
            //<your code>
            negative = false;
            string::const_iterator b = value.begin();
            string::const_iterator e = value.end();
            while(b != e) {
                if(*b == '-'){
                    negative = true;
				} else{
                    int digit = *b - '0';
                    container.insert(container.begin(), digit);   
				}
                ++b;				
            }
        }

        Integer             (const Integer&) = default;
        ~Integer            ()               = default;
        Integer& operator = (const Integer&) = default;

        // ----------
        // operator -
        // ----------

        /**
         * your documentation
         */
        Integer operator - () const {
            //your code
			*this.negative = !*this.negative;
            return this;
		} // fix

		// -----------
        // operator ++
        // -----------

        /**
         * your documentation
         */
        Integer& operator ++ () {
            *this += 1;
            return *this;}

        /**
         * your documentation
         */
        Integer operator ++ (int) {
            Integer x = *this;
            ++(*this);
            return x;}

        // -----------
        // operator --
        // -----------

        /**
         * your documentation
         */
        Integer& operator -- () {
            *this -= 1;
            return *this;}

        /**
         * your documentation
         */
        Integer operator -- (int) {
            Integer x = *this;
            --(*this);
            return x;}

        // -----------
        // operator +=
        // -----------

        /**
         * your documentation
         */
        Integer& operator += (const Integer& rhs) {
            // <your code>
			Integer<int> *result = new Integer<int>(0);
            Integer<int> ltemp = *this;
            Integer<int> rtemp = rhs;
            ltemp.abs();
			rtemp.abs();
			result->container.resize(this->container.size() + rhs.container.size(), 0);
			vector<int>::const_iterator b1 = this->container.begin();
			vector<int>::const_iterator e1 = this->container.end();
            vector<int>::const_iterator b2 = rhs.container.begin();
            vector<int>::const_iterator e2 = rhs.container.end();
            if(this->negative){
                if(!rhs.negative){
                    if(ltemp > rtemp){
                        minus_digits(b1, e1, b2, e2, result->container.begin());
			            result->negative = true;
					} else{
                        minus_digits(b2, e2, b1, e1, result->container.begin());
			            result->negative = false;
                    }
				} else{
                    plus_digits(b1, e1, b2, e2, result->container.begin());
			        result->negative = true;
				}
			} else{
                if(rhs.negative){
                    if(ltemp > rtemp){
                        minus_digits(b1, e1, b2, e2, result->container.begin());
			            result->negative = false;
					} else{
                        minus_digits(b2, e2, b1, e1, result->container.begin());
			            result->negative = true;
                    }
				} else{
                    plus_digits(b1, e1, b2, e2, result->container.begin());
			        result->negative = false;
				}
            }
            *this = *result;
            ostringstream delete_zeroes;
            delete_zeroes << *this;
            *this = Integer<int>(delete_zeroes.str());
            return *this;
		}

		// -----------
        // operator -=
        // -----------

        /**
         * your documentation
         */
        Integer& operator -= (const Integer& rhs) {
            // <your code>
            Integer<int> *result = new Integer<int>(0);
			result->container.resize(this->container.size() + rhs.container.size(),0);
			vector<int>::const_iterator b1 = this->container.begin();
			vector<int>::const_iterator e1 = this->container.end();
            vector<int>::const_iterator b2 = rhs.container.begin();
            vector<int>::const_iterator e2 = rhs.container.end();
            //6 Cases:
            if(this->negative){
                if(rhs.negative){
                    if(*this < rhs){
                        minus_digits(b1, e1, b2, e2, result->container.begin());                        
					    result->negative = true;
					} else{                        
                        minus_digits(b2, e2, b1, e1, result->container.begin());
					    result->negative = false;
                    }
				} else{                    
                    plus_digits(b1, e1, b2, e2, result->container.begin());
					result->negative = true;
                } 
			} else{
                if(!rhs.negative){
                    if(*this < rhs){
						//cout << "Hit1\n";
						minus_digits(b2, e2, b1, e1, result->container.begin());
						result->negative = true;
					} else{   
                        //cout << "Hit2\n";                     
                        minus_digits(b1, e1, b2, e2, result->container.begin());
					    result->negative = false;
                    }
				} else{        
                    //cout << "Hit3\n";            
                    plus_digits(b1, e1, b2, e2, result->container.begin());
					result->negative = false;
				}
            }            
			*this = *result;
            ostringstream delete_zeroes;
            delete_zeroes << *this;
            *this = Integer<int>(delete_zeroes.str());
            return *this;
        }

        // -----------
        // operator *=
        // -----------

        /**
         * your documentation
         */
        Integer& operator *= (const Integer& rhs) {
			bool change = false;
            if(this->negative != rhs.negative){
				change = true;
			}
			Integer<int> *result = new Integer<int>(0);
			result->container.resize(this->container.size() + rhs.container.size(),0);
			vector<int>::const_iterator b1 = this->container.begin();
			vector<int>::const_iterator e1 = this->container.end();
            vector<int>::const_iterator b2 = rhs.container.begin();
            vector<int>::const_iterator e2 = rhs.container.end();
            multiplies_digits(b1, e1, b2, e2, result->container.begin());
			*this = *result;
            if(change){
                this->negative = true;
			}            
            ostringstream delete_zeroes;
            delete_zeroes << *this;
            *this = Integer<int>(delete_zeroes.str());
			return *this;
        }

        // ------------
        // operator <<=
        // ------------

        /**
         * your documentation
         */
        Integer& operator <<= (int n) {
            // <your code>
            for (int i = 0; i < n; i++) {
                *this.container.insert(*this.container.begin(), 0);
            }
            return *this;}

        // ------------
        // operator >>=
        // ------------

        /**
         * your documentation
         */
        Integer& operator >>= (int n) {
            // <your code>
            for (int i = 0; i < n; i++) {
                *this.container.pop_back();
            }
            return *this;}

        // ---
        // abs
        // ---

        /**
         * absolute value
         * your documentation
         */
        Integer& abs () {
            // <your code>
            this->negative = false;
            return *this;}

        // ---
        // pow
        // ---

        /**
         * power
         * your documentation
         * @throws invalid_argument if ((this == 0) && (e == 0)) or (e < 0)
         */
        Integer& pow (int e) {
            // <your code>
            Integer<int> *temp = new Integer<int>(*this);
            for (int i = 1; i < e; i++) {
                *this *= *temp;
            }
            return *this;}

        string display() {
            vector<int>::const_iterator b = this->container.begin();
            vector<int>::const_iterator e = this->container.end();
            string result;
            if (this->negative) {
                result += '-';
            }
            while (--e != b) {
                result += *e + '0';
            }
            result += *e + '0';
            return result;
        }

        int to_integer() {
            string str = this->display();
            int i = stoi(str);
            return i;
        }
};
#endif // Integer_h
