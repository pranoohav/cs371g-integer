# CS371g: Generic Programming Integer Repo

* Names: Pranooha Veeramachaneni and Hogan Tran

* EID: pv5749 and htt425

* GitLab ID: pranoohav, hogan900

* HackerRank ID: pranooha, tranhogan

* Git SHA: fc33998d51495ff3addf2aae3542b2c8fc083e0b

* GitLab Pipelines: https://gitlab.com/pranoohav/cs371g-integer/-/pipelines

* Estimated completion time: 15 hours

* Actual completion time: (actual time in hours, int or float) 12 hours

* Comments: (any additional comments you have)
