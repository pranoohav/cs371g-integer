// --------------
// RunInteger.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, istream, ostream

#include "Integer.hpp"

using namespace std;

// ------------
// integer_read
// ------------

tuple<Integer<int>, string, Integer<int>> integer_read(istream_iterator<string> &begin_iterator) {
    Integer<int> first_val = Integer<int>(*begin_iterator);
    ++begin_iterator;
    string op = *begin_iterator;
    ++begin_iterator;
    Integer<int> second_val = Integer<int>(*begin_iterator);
    ++begin_iterator;
    return make_tuple(first_val, op, second_val);
}

// ------------
// integer_eval
// ------------

Integer<int> integer_eval(const tuple<Integer<int>, string, Integer<int>> t) {
    Integer<int> first_val = Integer<int>(0);
    string op;
    Integer<int> second_val = Integer<int>(0);
    Integer<int> result = Integer<int>(0);
    tie(first_val, op, second_val) = t;
    if (op.compare("+") == 0) {
        result = first_val + second_val;
    } else if (op.compare("-") == 0) {
        result = first_val - second_val;
    } else if (op.compare("*") == 0) {
        result = first_val * second_val;
    } else if (op.compare("**") == 0) {
        result = first_val.pow(second_val.to_integer());
    }
    return result;
}

// -------------
// integer_print
// -------------

void integer_print(ostream &sout, Integer<int> value) { 
    sout << value << endl;
}

// -------------
// integer_solve
// -------------

void integer_solve(istream &sin, ostream &sout) {
    istream_iterator<string> begin_iterator(sin);
    istream_iterator<string> end_iterator;
    while (begin_iterator != end_iterator) {
        integer_print(sout, integer_eval(integer_read(begin_iterator)));
    }
}

// ----
// main
// ----

int main () {
    // <your code>
    integer_solve(cin, cout);
    return 0;}
